//
//  Services.swift
//  AsyncApp
//
//  Created by Paul,David on 12/22/21.
//

import UIKit

private let OneSecond: UInt64 = 1000000000

private extension UIColor {
    static var random: UIColor {
        [UIColor.red,
         UIColor.orange,
         UIColor.blue,
         UIColor.green,
         UIColor.purple,
         UIColor.white,
         UIColor.gray,
         UIColor.black,
         UIColor.systemPink,
         UIColor.lightGray].randomElement()!
    }
}

class ColorNetworkService {
    func getColor() async -> UIColor {
        try? await Task.sleep(nanoseconds: OneSecond)
        return .random
    }

    func getAnotherColor() async -> UIColor {
        try? await Task.sleep(nanoseconds: OneSecond)
        return .random
    }
}

class ColorCoordinatorService {
    let service = ColorNetworkService()

    func getColors() async -> (UIColor, UIColor) {
        async let color = service.getColor()
        async let anotherColor = service.getAnotherColor()

        return await (color, anotherColor)
    }
}
