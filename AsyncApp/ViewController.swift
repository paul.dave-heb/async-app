//
//  ViewController.swift
//  AsyncApp
//
//  Created by Paul,David on 12/22/21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var button: UIButton!
    let service = ColorCoordinatorService()

    var someNumber = 2

    override func viewDidLoad() {
        super.viewDidLoad()
        button.addTarget(self, action: #selector(didTap), for: .touchUpInside)
    }

    @objc func didTap() {
        Task {
            print(someNumber)
            button.isEnabled = false
            let colors = await service.getColors()
            view.backgroundColor = colors.0
            button.backgroundColor = colors.1

            print(someNumber)

            // new continuation
            button.isEnabled = true
        }
    }

    @objc func someOtherButtonTap() {
        someNumber += 1
    }
}

